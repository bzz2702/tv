//
//  AppDependency.swift
//  TV
//
//  Created by Milan Banjanin on 21/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import Foundation

struct AppDependency {
    var dataManager: DataManager?
    
    init(dataManager: DataManager? = nil) {
        self.dataManager = dataManager
    }
}

protocol NeedsDependency: class {
    var dependencies: AppDependency? { get set }
}
