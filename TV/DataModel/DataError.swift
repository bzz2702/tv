//
//  DataError.swift
//  TV
//
//  Created by Milan Banjanin on 21/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import Foundation

enum DataError: Error {
    case tmdbError(TMDbError)
    case noData
    case genericError(Swift.Error)
}
