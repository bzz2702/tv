//
//  DataManager.swift
//  TV
//
//  Created by Milan Banjanin on 21/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import Foundation

final class DataManager {
    
    private var tmdb = TMDb()
    
    private var searchResults: [String: [SearchResult]] = [:]
    private(set) var recentSearches: Set<String> = []
    private var fileName: String = "recentSearches.txt"
    private var recentSearchesSeparator: String = "|"
    
    init() {
        readRecentSearches()
    }
    
}

extension DataManager {
    
    func search(for term: String, type: TMDb.SearchType, page: Int, callback: @escaping ([SearchResult], DataError?) -> Void) {
        
        callback(searchResults[term] ?? [], nil)
        
        let path: TMDb.Path = .search(term: term, type: type, page: page)
        tmdb.call(path: path) {
            data, tmdbError in
            
            if let tmdbError = tmdbError {
                callback([], .tmdbError(tmdbError))
                return
            }
            
            guard let data = data else {
                callback([], .noData)
                return
            }
            
            self.performSaveRecentSearches()
            self.recentSearches.insert(term.localizedLowercase)
            
            switch type {
                
            case .movie:
                let decoder = JSONDecoder()
                do {
                    let moviesResponse = try decoder.decode(MoviesResponse.self, from: data)
                    callback(moviesResponse.results, nil)
                } catch let error {
                    callback([], .genericError(error))
                }
            
            case .tv:
                let decoder = JSONDecoder()
                do {
                    let tvShowsResponse = try decoder.decode(TVShowsResponse.self, from: data)
                    callback(tvShowsResponse.results, nil)
                } catch let error {
                    callback([], .genericError(error))
                }
                
            case .person:
                let decoder = JSONDecoder()
                do {
                    let personResponse = try decoder.decode(PersonResponse.self, from: data)
                    callback(personResponse.results, nil)
                } catch let error {
                    callback([], .genericError(error))
                }
            }
            
            // return back to UI
            // callback([], nil)
        }
    }
    
    func populars(_ type: TMDb.SearchType, _ page: Int, callback: @escaping ([SearchResult], DataError?) -> Void) {
        
        let path: TMDb.Path = .popular(type: type, page: page)
        tmdb.call(path: path) {
            data, tmdbError in
            
            if let tmdbError = tmdbError {
                callback([], .tmdbError(tmdbError))
                return
            }
            
            guard let data = data else {
                callback([], .noData)
                return
            }
            
            switch type {
            case .movie:
                let decoder = JSONDecoder()
                do {
                    let moviesResponse = try decoder.decode(MoviesResponse.self, from: data)
                    callback(moviesResponse.results, nil)
                } catch {
                    print(error.localizedDescription)
                }
            case .tv:
                let decoder = JSONDecoder()
                do {
                    let tvShowsResponse = try decoder.decode(TVShowsResponse.self, from: data)
                    callback(tvShowsResponse.results, nil)
                } catch {
                    print(error.localizedDescription)
                }
            case .person:
                let decoder = JSONDecoder()
                do {
                    let personResponse = try decoder.decode(PersonResponse.self, from: data)
                    callback(personResponse.results, nil)
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }
    
    
    func fetchMovieDetails(movie: Movie, callback: @escaping (Movie?, DataError?) -> Void) {
        
        let path: TMDb.Path = .movie(movieId: movie.id)
        tmdb.call(path: path) {
            data, tmdbError in
            
            if let tmdbError = tmdbError {
                callback(nil, .tmdbError(tmdbError))
                return
            }
            
            guard let data = data else {
                callback(nil, .noData)
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let movieResponse = try decoder.decode(Movie.self, from: data)
                callback(movieResponse, nil)
            } catch {
                print(error.localizedDescription)
            }
            
        }
    }
    
    func fetchTVShowDetails(tvShow: TVShow, callback: @escaping (TVShow?, DataError?) -> Void) {
        let path: TMDb.Path = .tvShow(tvShowId: tvShow.id)
        tmdb.call(path: path) {
            data, tmdbError in
            
            if let tmdbError = tmdbError {
                callback(nil, .tmdbError(tmdbError))
                return
            }
            
            guard let data = data else {
                callback(nil, .noData)
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let tvShowResponse = try decoder.decode(TVShow.self, from: data)
                callback(tvShowResponse, nil)
            } catch {
                print(error.localizedDescription)
            }
            
        }
    }
    
    func fetchPersonDetails(person: Person, callback: @escaping (Person?, DataError?) -> Void) {
        let path: TMDb.Path = .person(personId: person.id)
        tmdb.call(path: path) {
            data, tmdbError in
            
            if let tmdbError = tmdbError {
                callback(nil, .tmdbError(tmdbError))
                return
            }
            
            guard let data = data else {
                callback(nil, .noData)
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let personResponse = try decoder.decode(Person.self, from: data)
                callback(personResponse, nil)
            } catch {
                print(error.localizedDescription)
            }
            
        }
    }
}

extension DataManager {
    
    private func readRecentSearches() {
        let fm = FileManager.default
        guard
            let folder = fm.applicationSupportURL
        else {
            return
        }
        
        let fileURL = folder.appendingPathComponent(fileName)
        
        DispatchQueue.global(qos: .utility).async {
            guard
                let data = fm.contents(atPath: fileURL.path),
                let s = String(data: data, encoding: .utf8)
                else {
                    return
            }
            
            let arr = s.components(separatedBy: self.recentSearchesSeparator)
            self.recentSearches = Set(arr)
        }
        
    }
    
    private func performSaveRecentSearches() {
        DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + 3) {
            [unowned self] in
            self.saveRecentSearches()
        }
    }
    
    private func saveRecentSearches() {
        let fm = FileManager.default
        guard
            let folder = fm.applicationSupportURL
            else {
                return
        }
        
        let fileURL = folder.appendingPathComponent(fileName)
        
        let s = recentSearches.joined(separator: recentSearchesSeparator)
        
        do {
            try s.write(to: fileURL, atomically: true, encoding: .utf8)
        } catch let fileError {
            print(fileError)
        }
        
    }
    
}
