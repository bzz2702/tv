//
//  Movie.swift
//  TV
//
//  Created by Milan Banjanin on 21/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit

//final class Movie {
//
//    var persons: [Person] = []
//
//}

struct Movie: Codable {
    
    let id: Int
    let title: String
    let posterPath: String? // optional since not all movies have a poster
    let overview: String
    let genreIDs: [Int]
    let releaseDate: String
    let rating: Double
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case posterPath = "poster_path"
        case overview
        case genreIDs = "genre_ids"
        case releaseDate = "release_date"
        case rating = "vote_average"
    }
}

extension Movie: SearchResult {
    
    var date: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'"
        guard let d = dateFormatter.date(from: releaseDate) else { return "" }
        let showDate = DateFormatter()
        showDate.dateFormat = "dd MMM, yyyy"
        let dd = showDate.string(from: d)
        return dd
    }
    
    var genres: String? {
        var arr: [String] = []
        for (id, value) in Genres.genresDict {
            if genreIDs.contains(id) {
                arr.append(value)
            }
        }
        return arr.joined(separator: ", ")
    }
    
    var name: String {
        return title
    }
    
    var imageURL: URL? {
        if let posterPath = posterPath {
           return URL(string: PosterPath.baseURL + PosterPath.imageSize + posterPath)
        }
        return nil
    }
    
    var imageURLHD: URL? {
        if let posterPath = posterPath {
            return URL(string: PosterPath.baseURL + PosterPath.imageSizeHD + posterPath)
        }
        return nil
    }
    
}

struct MoviesResponse: Codable {
    let results: [Movie]
    
}
