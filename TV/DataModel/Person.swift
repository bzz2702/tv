//
//  Person.swift
//  TV
//
//  Created by Milan Banjanin on 22/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import Foundation

struct Person: Codable {
    
    let id: Int
    let name: String
    let posterPath: String?
//    let knownFor: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case posterPath = "profile_path"
//        case knownFor
    }
}

extension Person: SearchResult {
    
    var date: String {
        return ""
    }
    
    var genres: String? {
        return ""
    }
    
    var overview: String {
        return ""
    }
    
    var imageURL: URL? {
        if let posterPath = posterPath {
            return URL(string: PosterPath.baseURL + PosterPath.imageSize + posterPath)
        }
        return nil
    }
    
    var imageURLHD: URL? {
        if let posterPath = posterPath {
            return URL(string: PosterPath.baseURL + PosterPath.imageSizeHD + posterPath)
        }
        return nil
    }
    
}

struct PersonResponse: Codable {
    let results: [Person]
}
