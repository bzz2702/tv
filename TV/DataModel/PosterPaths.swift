//
//  PosterPath.swift
//  TV
//
//  Created by Milan Banjanin on 21/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import Foundation

class PosterPath {
    static let baseURL = "https://image.tmdb.org/t/p/"
    static let imageSize = "w185"
    static let imageSizeHD = "w780"
}
