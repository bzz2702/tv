//
//  SearchResult.swift
//  TV
//
//  Created by Milan Banjanin on 21/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit

protocol SearchResult: Codable {
    var id: Int { get }
    var name: String { get }
    var imageURL: URL? { get }
    var overview: String { get }
    var genres: String? { get }
    var date: String { get }
}
