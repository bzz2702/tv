//
//  TVShow.swift
//  TV
//
//  Created by Milan Banjanin on 21/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import Foundation

struct TVShow: Codable {
    
    let id: Int
    let name: String
    let posterPath: String?
    let overview: String
    let genreIDs: [Int]
    let firstAirDate: String
    let rating: Double
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case posterPath = "poster_path"
        case overview
        case genreIDs = "genre_ids"
        case firstAirDate = "first_air_date"
        case rating = "vote_average"
    }
}

extension TVShow: SearchResult {
    
    var date: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'"
        guard let d = dateFormatter.date(from: firstAirDate) else { return "" }
        let showDate = DateFormatter()
        showDate.dateFormat = "dd MMM, yyyy"
        let dd = showDate.string(from: d)
        return dd
    }
    
    
    var genres: String? {
        var arr: [String] = []
        for (id, value) in Genres.genresDict {
            if genreIDs.contains(id) {
                arr.append(value)
            }
        }
        return arr.joined(separator: ", ")
    }
    
    
    var imageURL: URL? {
        if let posterPath = posterPath {
            return URL(string: PosterPath.baseURL + PosterPath.imageSize + posterPath)
        }
        return nil
    }
    
    var imageURLHD: URL? {
        if let posterPath = posterPath {
            return URL(string: PosterPath.baseURL + PosterPath.imageSizeHD + posterPath)
        }
        return nil
    }
    
}

struct TVShowsResponse: Codable {
    let results: [TVShow]
}
