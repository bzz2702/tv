//
//  TMDb.swift
//  TV
//
//  Created by Milan Banjanin on 21/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import Foundation

typealias JSON = [String: Any]

final class TMDb {
    
//    init() {}
    
    typealias Callback = (Data?, TMDbError?) -> Void
    
    func call(path: Path, callback: @escaping Callback) {
        let req = path.urlRequest
        
        let task = URLSession.shared.dataTask(with: req) {
            data, response, error in
            
            if let error = error {
                callback(nil, .someError(error))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                callback(nil, .someError(error as! URLError))
                return
            }
            
            if !(200..<300).contains(httpResponse.statusCode) {
                callback(nil, .someError(error as! URLError))
                return
            }
            
            guard let data = data else {
                callback(nil, .someError(error as! URLError))
                return
            }
            
            callback(data, nil)
        }
        task.resume()
    }
    
}

private extension TMDb {
    
    static let basePath: String = "https://api.themoviedb.org/3/"
    
    static let commonHeaders: [String: String] = {
        return [
            "User-Agent": "TV",
            "Accept-Charset": "utf-8",
            "Accept-Encoding": "gzip, deflate"
        ]
    } ()
    
}

extension TMDb {
    
    static let apiKey: String = "0bac158594aef8d4acc34478325d41c2"
    
    enum SearchType: String {
        case movie
        case tv // tv shows
        case person
        
        init?(index: Int) {
            switch index {
            case 0:
                self = .movie
            case 1:
                self = .tv
            case 2:
                self = .person
            default:
                return nil
            }
        }
    }
    
    enum Path {
        
        case search(term: String, type: SearchType, page: Int?)
        case movie(movieId: Int)
        case popular(type: SearchType, page: Int?)
        case tvShow(tvShowId: Int)
        case person(personId: Int)
        
        fileprivate var urlRequest: URLRequest {
            
            guard var comps = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
                fatalError("Invalid path-based URL")
            }
            
            comps.queryItems = queryItems
            
            guard let finalURL = comps.url else {
                fatalError("Invalid query items")
            }
        
            var req = URLRequest(url: finalURL)
            
            req.httpMethod = method
            req.allHTTPHeaderFields = headers
            
            return req
        }
        
        private var headers: [String: String] {
            let h = TMDb.commonHeaders // var h
    
            switch self {
            case .search, .movie, .popular, .tvShow, .person:
                break
            }
            
            return h
        }
        
        private var method: String {
            switch self {
            case .search, .movie, .popular, .tvShow, .person:
                return "GET"
            }
        }
        
        private var url: URL {
            
            guard let baseURL = URL(string: TMDb.basePath) else {
                fatalError("Invalid base URL")
            }
            
            
            switch self {
            case .search( _, let type, _):
                return baseURL.appendingPathComponent("search").appendingPathComponent(type.rawValue)
            case .movie(let movieId):
                return baseURL.appendingPathComponent("movie").appendingPathComponent("\(movieId)")
            case .tvShow(let tvShowId):
                return baseURL.appendingPathComponent("tv").appendingPathComponent("\(tvShowId)")
            case .person(let personId):
                return baseURL.appendingPathComponent("person").appendingPathComponent("\(personId)")
            case .popular(let type, _):
                return baseURL.appendingPathComponent(type.rawValue).appendingPathComponent("popular")

            }
//            fatalError("Failed to create proper URL")
        }
        
        private var params: [String: Any] {
            var p: [String: Any] = [:]
            
            switch self {
            case .search(let term, _, let page):
                p["api_key"] = TMDb.apiKey
                p["query"] = term
                if let page = page {
                    p["page"] = page
                }
            case .movie, .tvShow, .person:
                p["api_key"] = TMDb.apiKey
            case .popular(_, let page):
                if let page = page {
                    p["page"] = page
                }
                p["api_key"] = TMDb.apiKey
            }
            
            return p
        }
        
        private var queryItems: [URLQueryItem] {
            var arr: [URLQueryItem] = []
            for (key, value) in params {
                let qi = URLQueryItem(name: key, value: "\(value)")
                arr.append(qi)
            }
            return arr
        }
        
    }
    
}
