//
//  TMDbError.swift
//  TV
//
//  Created by Milan Banjanin on 21/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import Foundation

enum TMDbError: Error {
    case someError(Error)
}
