//
//  MovieController.swift
//  TV
//
//  Created by Milan Banjanin on 21/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit
import Kingfisher
import UICircularProgressRing

class MovieController: UIViewController, NeedsDependency {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var ratingRing: UICircularProgressRing!
    
    var dependencies: AppDependency?
    var movie: Movie!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchDetails()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        ratingRing.valueFormatter = UICircularProgressRingFormatter(valueIndicator: "/10", rightToLeft: false, showFloatingPoint: true, decimalPlaces: 1)
        ratingRing.startProgress(to: CGFloat(movie.rating), duration: 1.5)
    }
    
}

private extension MovieController {
    
    func fetchDetails() {
        guard let dataManager = dependencies?.dataManager else {
            fatalError("Missing data manager")
        }
        
        dataManager.fetchMovieDetails(movie: movie) {
            updatedMovie, dataError in
            self.movie = updatedMovie
        }
        
        imageView.kf.setImage(with: movie.imageURLHD)
        label.text = movie.name
        overviewLabel.text = movie.overview
        
    }
    
}
