//
//  PersonController.swift
//  TV
//
//  Created by Milan Banjanin on 30/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit
import Kingfisher

final class PersonController: UIViewController, NeedsDependency {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    var dependencies: AppDependency?
    var person: Person!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchDetails()
    }
}

private extension PersonController {
    
    func fetchDetails() {
        guard let dataManager = dependencies?.dataManager else {
            fatalError("Missing data manager")
        }
        
        dataManager.fetchPersonDetails(person: person) {
            updatedPerson, dataError in
            self.person = updatedPerson
        }
        
        imageView.kf.setImage(with: person.imageURLHD)
        label.text = person.name
        
    }
    
    
}
