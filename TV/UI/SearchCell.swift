//
//  SearchCell.swift
//  TV
//
//  Created by Milan Banjanin on 22/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit
import Kingfisher

final class SearchCell: UITableViewCell, ReusableView {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var ratingImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var overviewTextView: UITextView!

}

extension SearchCell {
    func populate(with item: SearchResult) {
        titleLabel.text = item.name
        posterImageView.kf.setImage(with: item.imageURL)
        overviewTextView.text = item.overview
        genresLabel.text = item.genres
        releaseDateLabel.text = item.date.description
    }
}
