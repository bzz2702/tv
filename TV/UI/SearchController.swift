//
//  SearchController.swift
//  TV
//
//  Created by Milan Banjanin on 21/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit

final class SearchController: UIViewController, NeedsDependency {
    
    // External dependency
    var dependencies: AppDependency?
    
    // UI
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var segmentedControl: UISegmentedControl!
    
    // Local data model
    private var searchTerm: String? {
        didSet {
            propertiesReset()
            
            if searchTerm == nil {
                showPopulars()
            } else if searchTerm == "" {
                showPopulars()
            } else {
                search()
            }
        }
    }
    
    var recentSearches: Set<String> = [] {
        didSet {
            orderedRecentSearches = recentSearches.sorted()
        }
    }
    
    private var orderedRecentSearches: [String] = []
    
    private var results: [SearchResult] = []
    
    private var searchType: TMDb.SearchType = .movie {
        didSet {
            propertiesReset()
            
            if searchTerm == nil {
                showPopulars()
            } else if searchTerm == "" {
                showPopulars()
            } else {
                search()
            }
        }
    }
    
    private var moviePage: Int = 1
    private var tvShowPage: Int = 1
    private var personPage: Int = 1
    private var page: Int {
        switch searchType {
        case .movie:
            return moviePage
        case .tv:
            return tvShowPage
        case .person:
            return personPage
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        searchBar.delegate = self
        showRecentSearches()
        showPopulars()

    }
    
}

private extension SearchController {
    
    @IBAction func changeSearchType(_ sender: UISegmentedControl) {
        guard let st = TMDb.SearchType(index: sender.selectedSegmentIndex) else { return }
        searchType = st
    }
    
    func search() {
        
        guard
            let dataManager = dependencies?.dataManager
        else {
            fatalError("Missing data manager")
        }
        
        guard
            let s = searchTerm
        else {
            return
        }
        
        dataManager.search(for: s, type: searchType, page: page) {
            [weak self] arr, error in
            
            // validate
            
            self?.results.append(contentsOf: arr)
            
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
    }
    
    func showPopulars() {
        guard
            let dataManager = dependencies?.dataManager
        else {
            fatalError("Missing data manager")
        }
        
        guard let st = TMDb.SearchType(index: segmentedControl.selectedSegmentIndex) else { return }

        
        dataManager.populars(st, page) {
            [weak self] arr, error in
            
            self?.results.append(contentsOf: arr)
            
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        switch st {
        case .movie:
            title = "Most popular movies"
        case .tv:
            title = "Most popular TV shows"
        case .person:
            title = "Most popular people"
        }
        
    }
    
}

extension SearchController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell: SearchCell = tableView.dequeueReusableCell(withIdentifier: SearchCell.reuseIdentifier, for: indexPath) as! SearchCell
        
        if indexPath.row == results.count - 1 {
            
            pageCounterCheck()
            
            if searchTerm == nil || searchTerm == "" {
                showPopulars()
                pageCounterIncrease()
            } else {
                search()
                pageCounterIncrease()
            }
            
        }
        let res = results[indexPath.row]
        cell.populate(with: res)
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let res = results[indexPath.row]
        
        switch searchType {
        case .movie:
            let storyboard = UIStoryboard(name: MovieController.storyboardName, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: MovieController.storyboardName) as! MovieController
            vc.movie = res as? Movie
            vc.dependencies = dependencies
            show(vc, sender: self)
        case .tv:
            let storyboard = UIStoryboard(name: TVShowController.storyboardName, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: TVShowController.storyboardName) as! TVShowController
            vc.tvShow = res as? TVShow
            vc.dependencies = dependencies
            show(vc, sender: self)
        case .person:
            let storyboard = UIStoryboard(name: PersonController.storyboardName, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: PersonController.storyboardName) as! PersonController
            vc.person = res as? Person
            vc.dependencies = dependencies
            show(vc, sender: self)
        }
        
    }
    
}

extension SearchController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchBar.endEditing(true)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchTerm = searchText
        if searchTerm != "" {
            title = "Showing results for '\(searchText)'"
        }
    }
    
    private func showRecentSearches() {

        searchBar.placeholder = recentSearches.compactMap{$0}.joined(separator: ", ")
        }
}

extension SearchController {
    
    func propertiesReset() {
        results = []
        moviePage = 1
        tvShowPage = 1
        personPage = 1
    }
    
    func pageCounterCheck() {
        if moviePage == 1 {
            moviePage += 1
        }
        if tvShowPage == 1 {
            tvShowPage += 1
        }
        if personPage == 1 {
            personPage += 1
        }
    }
    
    func pageCounterIncrease() {
        switch searchType {
        case .movie:
            moviePage += 1
        case .tv:
            tvShowPage += 1
        case .person:
            personPage += 1
        }
    }
    
}
