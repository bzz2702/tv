//
//  TVShowController.swift
//  TV
//
//  Created by Milan Banjanin on 30/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit
import Kingfisher
import UICircularProgressRing

final class TVShowController: UIViewController, NeedsDependency {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var ratingRing: UICircularProgressRing!
    
    var dependencies: AppDependency?
    var tvShow: TVShow!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchDetails()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        ratingRing.valueFormatter = UICircularProgressRingFormatter(valueIndicator: "/10", rightToLeft: false, showFloatingPoint: true, decimalPlaces: 1)
        ratingRing.startProgress(to: CGFloat(tvShow.rating), duration: 1.5)
    }
}

private extension TVShowController {
    
    func fetchDetails() {
        guard let dataManager = dependencies?.dataManager else {
            fatalError("Missing data manager")
        }
        
        dataManager.fetchTVShowDetails(tvShow: tvShow) {
            updatedTvShow, dataError in
            self.tvShow = updatedTvShow
        }
        
        imageView.kf.setImage(with: tvShow.imageURLHD)
        label.text = tvShow.name
        overviewLabel.text = tvShow.overview
        
    }
    
    
}


