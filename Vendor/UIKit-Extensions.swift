//
//  UIKit-Extensions.swift
//  TV
//
//  Created by Milan Banjanin on 22/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit

protocol ReusableView {
    static var reuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UIViewController {
    static var storyboardName: String {
        return String(describing: self)
    }
}
